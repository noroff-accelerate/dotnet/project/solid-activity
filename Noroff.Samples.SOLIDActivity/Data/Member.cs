﻿namespace Noroff.Samples.SOLIDActivity.Data
{
    /// <summary>
    /// Data Model for a Member.
    /// </summary>
    public class Member
    {
        public string Name { get; set; }
        public string MemberId { get; set; }
    }

}