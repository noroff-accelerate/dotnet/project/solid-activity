﻿namespace Noroff.Samples.SOLIDActivity.Data
{
    /// <summary>
    /// Data Model for a Book.
    /// </summary>
    public class Book
    {
        public string Title { get; set; }
        public string Author { get; set; }
    }

}